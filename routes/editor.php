<?php

/*
|--------------------------------------------------------------------------
| Editor Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Editor routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "web", "auth" and "Editor" middleware groups. Enjoy building your Editor!
|
*/

/**
 * Editor url
 */
Route::get('/', ['uses' => 'EditorController@index', 'as' => 'editor.index']);
Route::get('/settings', ['uses' => 'EditorController@settings', 'as' => 'editor.settings']);
Route::post('/settings', ['uses' => 'EditorController@saveSettings', 'as' => 'editor.save-settings']);
Route::post('/upload/image', ['uses' => 'ImageController@uploadImage', 'as' => 'upload.image']);
Route::delete('/delete/file', ['uses' => 'FileController@deleteFile', 'as' => 'delete.file']);
Route::post('/upload/file', ['uses' => 'FileController@uploadFile', 'as' => 'upload.file']);


/**
 * Editor uri
 */
Route::get('/posts', ['uses' => 'EditorController@posts', 'as' => 'editor.posts']);
Route::get('/images', ['uses' => 'ImageController@images', 'as' => 'editor.images']);
Route::get('/images-list', ['uses' => 'ImageController@images_list', 'as' => 'editor.images-list']);
Route::get('/files', ['uses' => 'FileController@files', 'as' => 'editor.files']);
Route::get('/app', ['uses' => 'AppController@index', 'as' => 'editor.app']);
Route::post('/app/email', ['uses' => 'AppController@sendMail', 'as' => 'editor.app.send-mail']);

/**
 * comment
 */
Route::post('/comment/{comment}/restore', ['uses' => 'CommentController@restore', 'as' => 'comment.restore']);
Route::match(['get', 'post'], '/comment/{comment}/verify', ['uses' => 'CommentController@verify', 'as' => 'comment.verify']);
Route::delete('comment/un-verified', ['uses' => 'CommentController@deleteUnVerifiedComments', 'as' => 'comment.delete-un-verified']);

/***
 * post
 */

Route::post('/post/{post}/restore', ['uses' => 'PostController@restore', 'as' => 'post.restore']);
Route::get('/post/{slug}/preview', ['uses' => 'PostController@preview', 'as' => 'post.preview']);
Route::post('/post/{post}/publish', ['uses' => 'PostController@publish', 'as' => 'post.publish']);
Route::get('/post/{post}/download', ['uses' => 'PostController@download', 'as' => 'post.download']);
Route::post('/post/{post}/config', ['uses' => 'PostController@updateConfig', 'as' => 'post.config']);
Route::get('/post/download-all', ['uses' => 'PostController@downloadAll', 'as' => 'post.download-all']);

/**
 * Editor resource
 */
Route::resource('post', 'PostController', ['except' => ['show', 'index']]);
Route::resource('category', 'CategoryController', ['except' => ['index', 'show', 'create']]);
Route::resource('tag', 'TagController', ['except' => ['index', 'show', 'create']]);
Route::resource('page', 'PageController', ['except' => ['show', 'index']]);

/**
 * IPS
 */
Route::delete('/ip/{ip}/toggle', ['uses' => 'IpController@toggleBlock', 'as' => 'ip.block']);
Route::delete('/ip/{ip}', ['uses' => 'IpController@destroy', 'as' => 'ip.delete']);
Route::delete('/ip', ['uses' => 'IpController@deleteUnBlocked', 'as' => 'ip.delete-unblocked']);

/**
 * failed jobs
 */

Route::delete('/failed-jobs', ['uses' => 'EditorController@flushFailedJobs', 'as' => 'Editor.failed-jobs.flush']);