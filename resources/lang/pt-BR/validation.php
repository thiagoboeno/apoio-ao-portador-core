<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'O atributo :attribute deve ser aceito..',
    'active_url'           => 'O atributo :attribute não é uma URL válida.',
    'after'                => 'O atributo :attribute deve ser uma data após :date.',
    'alpha'                => 'O atributo :attribute pode conter apenas letras.',
    'alpha_dash'           => 'O atributo :attribute pode conter apenas letras, números e traços.',
    'alpha_num'            => 'O atributo :attribute pode conter apenas letras e números.',
    'array'                => 'O atributo :attribute deve ser um array.',
    'before'               => 'O atributo :attribute deve ser uma data antes de :date.',
    'between'              => [
        'numeric' => 'O atributo :attribute deve estar entre :min e :max.',
        'file'    => 'O atributo :attribute deve estar entre :min e :max kilobytes.',
        'string'  => 'O atributo :attribute deve estar entre :min e :max caracteres.',
        'array'   => 'O atributo :attribute deve ter entre :min e :max itens.',
    ],
    'boolean'              => 'O campo :attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação do atributo :attribute não corresponde.',
    'date'                 => 'O atributo :attribute não é uma data válida.',
    'date_format'          => 'O atributo :attribute não corresponde ao formato :format.',
    'different'            => 'Os atributos :attribute e :other devem ser diferentes.',
    'digits'               => 'O atributo :attribute deve ter :digits digitos.',
    'digits_between'       => 'O atributo :attribute deve estar entre :min e :max digitos.',
    'dimensions'           => 'O atributo :attribute possui dimensões de imagem inválidas.',
    'distinct'             => 'O campo :attribute tem um valor duplicado.',
    'email'                => 'O atributo :attribute deve ser um endereço de email válido.',
    'exists'               => 'O atributo selecionado :attribute é inválido.',
    'file'                 => 'O atributo :attribute deve ser um arquivo.',
    'filled'               => 'O campo :attribute é obrigatório.',
    'image'                => 'O atributo :attribute deve ser uma imagem.',
    'in'                   => 'O atributo selecionado :attribute é inválido.',
    'in_array'             => 'O campo :attribute não existe em :other.',
    'integer'              => 'O atributo :attribute deve ser um número inteiro.',
    'ip'                   => 'O atributo :attribute deve ser um endereço IP válido.',
    'json'                 => 'O atributo :attribute deve ser uma sequência JSON válida.',
    'max'                  => [
        'numeric' => 'O atributo :attribute não pode ser maior que :max.',
        'file'    => 'O atributo :attribute não pode ser maior que :max kilobytes.',
        'string'  => 'O atributo :attribute não pode ser maior que :max caracteres.',
        'array'   => 'O atributo :attribute pode não ter mais que :max itens.',
    ],
    'mimes'                => 'O atributo :attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => 'O atributo :attribute deve ser pelo menos :min.',
        'file'    => 'O atributo :attribute deve ser pelo menos :min kilobytes.',
        'string'  => 'O atributo :attribute deve ser pelo menos :min caracteres.',
        'array'   => 'O atributo :attribute deve ter pelo menos :min itens.',
    ],
    'not_in'               => 'O atributo setecionado :attribute é inválido.',
    'numeric'              => 'O atributo :attribute deve ser um número.',
    'present'              => 'O campo do atributo :attribute deve estar presente.',
    'regex'                => 'O formato do atributo :attribute é inválido.',
    'required'             => 'O campo de atributo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_unless'      => 'O campo :attribute é obrigatório, a menos que :other esteja em :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values estiver presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estiver presente.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values ​​não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum dos :values estiver presente.',
    'same'                 => 'O atributo :attribute e :other devem corresponder.',
    'size'                 => [
        'numeric' => 'O atributo :attribute deve ser :size.',
        'file'    => 'O atributo :attribute deve ser :size kilobytes.',
        'string'  => 'O atributo :attribute deve ser :size caracteres.',
        'array'   => 'O atributo :attribute deve conter :size itens.',
    ],
    'string'               => 'O atributo :attribute deve ser uma string.',
    'timezone'             => 'O atributo :attribute deve ser uma zona válida.',
    'unique'               => 'O atributo :attribute já foi utilizado.',
    'url'                  => 'O formato do atributo :attribute é inválido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

];
