<?php

return array(
    //view/view/index.blade.php
    'MY_BLOG'=>'Apoio Portador',
    'POST'=>'Posts',
    'CLICK_SEE_LIST'=>'Clique para ver a lista de vagas do site',
    'BLOG'=>'Apoio Portador',
    'CHECK'=>'Vizualizar',
    '`S' => '`s`',
    'ARCHIVE' => 'Linha do Tempo',
    'NO_POSTS' => 'Não há vagas.',
    'CLASSIFICATION' => 'Categoria',
    'ITEM' => 'item',
    //view/view/projects.blade.php
    'LOADING' => 'Carregando...',
    //view/search.blade.php
    'SEARCH' => 'Presquisar',
    'SEARCH_NOTHING'=>'Não foram encontrado resultados...',
    //view/admin/category/edit.blade.php
    'CATEGORY_NAME' => 'Nome da Categoria',
    'CATEGORY_DESCRIPTION' => 'Descrição da Categoria',
    'CATEGORY_IMG'=>'Imagem da Categoria',
    'DELETE' => 'Deletar',
    'EDIT' => 'Editar',
    'EDIT2' => 'Editar',
    //view/admin/layouts/app.blade.php
    'GO_TO_INDEX' => 'Voltar para a página principal',
    //view/admin/modals/add-category-modal.blade.php
    'NEW_CATEGORY' => 'Nova Categoria',
    'CANCEL' => 'Cancelar',
    'CONFIRM' => 'Confirmar',
    //view/admin/modals/add-tag-modal.blade.php
    'NEW_TAG'=>'Nova Tag',
    'TAG_NAME' => 'Nome da Tag',
    //view/admin/page/create.blade.php
    'CREATE' => 'Criar',
    //view/admim/page/form-content.blade.php
    'FORM_CONTENT_URI' => 'Uri da página',
    'FORM_CONTENT_NAME' => 'Nome da página',
    'FORM_CONTENT' => 'Conteúdo da página',
    'COMMENT_MESSAGE' => 'Comentário',
    'OTHER_MESSAGE' => 'Outros comentários',
    'DEFAULT' => 'Padrão',
    'FORCE_DISABLE'=>'Forçar a fechar',
    'FORCE_ENABLE'=>'Forçar a Abrir',
    'COMMENT_TYPE'=>'Tipo de Comentário',
    'COMMENT_RAW' => 'Comentário bruto',
    'ALLOW_RESOURCE_COMMENT' => 'Ativar para comentar',
    'DISALLOW_COMMENT' => 'Não permitir comentar',
    'ALLOW_COMMENT' => 'Permitir comentar',
    'DISPLAY_AT_INDEX' => 'Mostrar no índice',
    'HIDE_AT_INDEX' => 'Não mostrar no índice',
    'ORDER'=>'Ordem',
    //view/admin/partials/image_list.blade.php
    'ORIGINAL_IMAGE' => 'Imagem original',
    'COPY' => 'Copiar',
    'CONFIRM_TO_REMOVE'=>'Confirme para remover',
    'REMOVE' => 'Remover',
    'NO_IMAGE'=>'Não há imagens',
    //view/admin/partials/ip_button.blade.php
    'BLOCK' => 'Bloquear',
    'UNBLOCK'=>'Desbloquear',
    'BLOCK_DESCRIPTION' => 'Este IP não poderá acessar seu site após o bloqueio',
    //view/admin/post/form-content.blade.php
    'COVER_IMAGE'=>'Imagem de capa',
    'ARTICLE_TITLE'=>'Título da Vaga',
    'ARTICLE_DESCRIPTION'=>'Descrição da Vaga',
    'ARTICLE_SLUG' => 'Url da Vaga',
    'ARTICLE_CATEGORY'=>'Categoria da Vaga',
    'ARTICLE_TAGS'=>'Tag da Vaga',
    'ARTICLE_CONTENT'=>'Conteúdo da Vaga',
    'USE_MARKDOWN_WRITE' => 'Por favor, escreva no formato Markdown',
    'FORCE_DISABLE_COMMENT' => 'Forçado a mostrar comentários',
    'FORCE_ENABLE_COMMENT' => 'Forçado a mostrar comentários',
    'IS_SHOW_TOC' => 'Se deve exibir o sumário',
    'CLOSE'=>'Fechar',
    'SHOW' =>'Exibir',
    'PUBLISH'=> 'Publicar',
    'DRAFT'=>'Rascunho',
    //view/admin/categories.blade.php
    'NAME'=>'Nome',
    'DESCRIPTION' => 'Descrição',
    'ARTICLE' => 'Vaga',
    'OPERATING'=>'Operador',
    //view/admin/comments.blade.php
    'UNVERIFIED_COMMENT'=>'Remover revisão do comentário?',
    'DELETE_UNVERIFIED'=>'Excluir comentários não revisado',
    'USER'=>'Usuário',
    'E_MAIL' => 'Email',
    'ADDRESS' => 'Endereço',
    'STATUS' => 'Status',
    'IS_REMOVE'=>'Excluído',
    'IS_VERIFIED'=>'Auditado',
    'NOT_VERIFIED'=>'Não revisado',
    'DELETE_FOREVER'=>'Detetar Permanentemente',
    'DELETE_THIS_COMMENT_FOREVER'=>'Excluir este comentário permanentemente',
    'RECOVERY'=>'Restaurar',
    'SURE_DELETE_THIS_COMMENT'=>'Certifique-se de excluir este comentário',
    //view/admin/files.blade.php
    'DOCUMENT'=>'Documento',
    'SURE_TO_DELETE'=>'Confirme para deletar',
    //view/admin/index.blade.php
    'PAGE'=>'Página',
    'CATEGORY'=>'Categoria',
    'IMAGE'=>'Imagem',
    //view/admin/ips.blade.php
    'COMMENT_NUM'=>'Número de Comentários',
    //view/admin/posts.blade.php
    'TITLE'=>'Título',
    'UNPUBLISHED'=>'Não publicado',
    'PUBLISHED' => 'Publicado',
    'REVOKE'=>'Revogar',
    'REVIEW'=>'Review',
    'REMOVE_TIPS' => 'Excluir (isso será excluído permanentemente)',
    'HIDE_COMMENT' =>'Não exibir comentários',
    'DISPLAY_COMMENT' => 'Exibir comentários',
    //view/admin/settings.blade.php
    'SAVE'=>'Salvar',
    //view/admin/user.blade.php
    'SIGN_UP_DATE' => 'Data de registro',
    'SOURCE'=>'Fonte',
    //view/auth/github_register.blade.php
    'NICK_NAME'=>'Apelido',
    'CONFIRM_PASSWORD' => 'Confirmar senha',
    'SIGN_UP' => 'Cadastrar-se',
    //view/auth/login.blade.php
    'PASSWORD'=>'Senha',
    'REMEMBER_PASSWORD'=>'Lembrar senha',
    'FORGOT_PASSWORD'=>'Esqueci minha senha',
    'USE'=>'Usar ',
    //view/comment/comment.blade.php
    'BLOGGER'=>'Apoio Portador',
    //view/comment/show.blade.php
    'NO_COMMENT'=>'No comment yet',
    //view/post/show.blade.php
    'INDEX'=>'Index',
    'COPYRIGHT'=>'Copyright Notice',
    'COPYRIGHT_STATEMENT1'=>'Free reprint - non-commercial - non-derivative - keep signature',
    'COPYRIGHT_STATEMENT2'=>'Creative Sharing 3.0 License',
    'CREATE_DATE'=>'Data de Criação',
    'EDIT_DATE'=>'Data de Modificação',
    'READ_NUM'=>'Visualizações',
    //view/user/notifications.blade.php
    'COMMENTS' => 'Comentários',
    'COMMENT' => 'Comentário',
    'MENTIONED_COMMENT'=>'Mencionou você',
    'BASE_NOTIFICATION'=>'Lembrete básico',
    //view/user/pictures.blade.php
    'CHANGE_USER_IMG'=>'Modificar imagem do usuário',
    'CHANGE_INFO_IMG'=>'Modificar informações da imagem',
    //view/user/render-notification.blade.php
    'READ_NOTIFICATION'=>'Lido',
    'READ_ALL_NOTIFICATION'=>'Tudo lido',
    'DELETE_READ'=>'Deletar lidos',
    //view/user/settings.blade.php
    'REAL_NAME'=>'Nome Atual',
    'NULL'=>'nulo',
    //view/user/socials.blade.php
    'BIND'=>'Ligar',
    'IS_BIND'=>'Obrigatório',
    //view/layouts/header.blade.php
    'ACCOUNT_CENTER' => 'Dashboard',
    'ADMIN_AREA' => 'Admin',
    'EDITOR_AREA' => 'Editor',
    'NOTIFICATION' => 'Notificações',
    'LOGOUT' => 'Deslogar',
    'LOGIN' => 'Logar',
    'POP_ARTICLES' => 'Vagas populares',
    'VIEWS' => 'Volume de leitura',
    //view/pay.blade.php
    'WRITE_WELL_DONATE' => 'Written well, sponsor the hosting fee',
    'DONATE' => 'Donate',
    'ZHIFUBAO' => 'Alipay',
    'WECHAT' => 'WeChat',
    'ZHIFUBAO_TABPANEL' => 'Scan and appreciate with Alipay',
    'WECHAT_TABPANEL' => 'Scan and appreciate with WeChat',
    //view/raw_comment.blade.php
    'USERNAME' => 'Nome de Usuário',
    'YOUR_NAME'=>'Seu Nome',
    'EMAIL' => 'E-mail',
    'YOUR_EMAIL' => 'O e-mail não será exibido',
    'WEBSITE' => 'Site',
    'YOUR_WEBSITE' => 'Opcional, preencha e clique no avatar para entrar diretamente',
    'COMMENT_ARTICLE'=>'Comentários',
    'SUPPORT'=>'Suporte',
    'REVIEW_N_SHOW' => 'Não será exibido até depois da revisão',
    'COMMENT_SUBMIT' => 'Resposta',
    //view/recommended_posts.blade.php
    'RECOMMENDED_POSTS'=>'Vagas recomentadas',
    //view/tags.balde.php
    'TAG'=>'Tag',
    'NO_TAGS'=>'Não há tags.',
    //view/archieve.blade.php
    'TOTAL' => 'Total',
    'TOTAL_POSTS' => 'Vagas',

    //controller/AdminController.php
    'SAVE_SUCCESS'=>'Salvo com sucesso',
    'SAVE_FAIl'=>'\'Falha ao salvar\'',
    //controller/CategoryController.php
    'HAVE_POST_CANT_REMOVE'=>'As vagas não podem ser removidas',
    //controller/FileController.php
    'UPLOAD_SUCCESS'=>'Envio feito com sucesso',
    'UPLOAD_FAIL'=>'Falha ao fazer o envio',
    //controller/PostController.php
    'PLEASE_CREATE_CATEGORY'=>'Por favor, crie uma categoria',
    'CREATE_SUCCESS'=>'Criada com sucesso',
    'CREATE_FAIL'=>'Falha ao criar',
    'PUBLISH_FAIL_REMOVE'=>'Falha na publicação. Por favor, recuperação remover item ',
    'PUBLISH_SUCCESS'=>'Publicar sucesso',
    'REVOKE_PUBLISH_SUCCESS'=>'Revogar o sucesso do publicador.',
    'OPERATING_FAIL'=>'Falha na operação',
    'EDIT_SUCCESS'=>'Sucesso ao editar',
    'EDIT_FAIL'=>'Falha ao editar',
    'RECOVERY_SUCCESS'=>'Sucesso ao recuperar',
    'RECOVERY_FAIL'=>'Falha ao recuperar',
    'SUCCESS'=>'Sucesso',
    'REMOVE_SUCCESS'=> 'Removido com sucesso',
    'REMOVE_FAIL' => 'Falha ao remover',
    //controller/Auth/AuthController.php
    'USE_GITHUB_SING_UP_SUCCESS'=>'Sucesso ao usar o GitHub para registrar-se',
    'USE_GITHUB_SING_UP_FAIL'=>'Falha ao usar o GitHub para registrar-se',
    'BIND_GITHUB_SUCCESS'=>'Conta do GitHub linkada com sucesso',
    'BIND_GITHUB_FAIL'=>'Falha ao linkar conta do GitHub',
    //controller/HomeController.php
    'PLEASE_KEY_IN_KEYWORD'=>'Digite as palavras-chave',
    'PLEASE_KEY_IN_URL_OR_IMG'=>'Digite o URL ou faça o upload da imagem.'
);