@extends('layouts.home')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-12 banner text-center text-white">
            <div class="row">
                <div class="col-12 col-md-6 pl-auto py-5">
                    <video width="100%" height="450" controls>
                        <source src="https://www.youtube.com/watch?v=0_JW5EmXfhs" type="video/mp4">
                        Seu navegador não suporta o vídeo
                    </video>
                </div>
                <div class="col-12 col-md-6 slogan text-white">
                    <h1 class="mb-5">Apoio Portador</h1>
                    <h5>
                        Mostrando o verdadeiro mundo aos PCD's.
                    </h5>
                    <h5>
                        Conectando empresas ao melhor do mercado e gerando sorrisos.
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <div class="row justify-content-around bg-home py-4">
        <div class="col-12 text-center text-dados py-4">
            <h1>Dados</h1>
        </div>
        <div class="col-12 col-md-4 d-flex flex-column justify-content-center align-items-center">
            <div class="round-dados">
                <i class="fa fa-user"></i>
            </div>
            <h1 class="text-dados">Usuários</h1>
            <h2 class="text-dados">{{ $users }}</h2>
        </div>
        <div class="col-12 col-md-4 d-flex flex-column justify-content-center align-items-center">
            <div class="round-dados">
                <i class="fa fa-building-o"></i>
            </div>
            <h1 class="text-dados">Empresas</h1>
            <h2 class="text-dados">{{ $enterprises }}</h2>
        </div>
        <div class="col-12 col-md-4 d-flex flex-column justify-content-center align-items-center">
            <div class="round-dados">
                <i class="fa fa-suitcase"></i>
            </div>
            <h1 class="text-dados">Vagas</h1>
            <h2 class="text-dados">{{ $jobs }}</h2>
        </div>
    </div>

    <div class="row">
            <div class="col-12 banner text-center text-white">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <img src="img/banner.jpeg" alt="">
                        </div>
                    </div>
                    <div class="col-12 col-md-6 slogan text-white">
                        <h1 class="mb-5">Sobre Nós</h1>
                        <h5 class="mb
                        -5">
                            A Apoio Potador nasceu em 2019 no Colégio Cotemig, em Belo Horizonte, quando 5 alunos tiveram a ideia de desenvolver uma plataforma que interligasse pessoas com alguma deficiência com empresas que estão em busca desses PCD's para fazer parte de suas equipes.
                        </h5>
                        <h5>
                            Hoje, a plataforma conta com {{ $totalUsers }} clientes ativos, e busca crescimento para cada vez mais entregar conexões e profissiionais qualificados!!
                        </h5>
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection