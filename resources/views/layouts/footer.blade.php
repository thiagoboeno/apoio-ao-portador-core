<footer class="footer" id="footer">
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          @if(isset($case_number)) <span>{{ $case_number }}</span> | @endif
          <span>Feito com carinho pela Apoio Portador </span>|
          <img
            width="20"
            src="{{ url('/img/apoio-portador-logo.png') }}"
            alt="Feed"
          />
        </div>
      </div>
    </div>
  </div>
</footer>
