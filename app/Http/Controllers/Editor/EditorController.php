<?php

namespace App\Http\Controllers\Editor;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Http\Repositories\CategoryRepository;
use App\Http\Repositories\CommentRepository;
use App\Http\Repositories\ImageRepository;
use App\Http\Repositories\MapRepository;
use App\Http\Repositories\PageRepository;
use App\Http\Repositories\PostRepository;
use App\Http\Repositories\TagRepository;
use App\Http\Repositories\UserRepository;
use App\Page;
use App\Post;
use Carbon\Carbon;
use DB;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class EditorController extends Controller
{
    protected $postRepository;
    protected $commentRepository;
    protected $userRepository;
    protected $tagRepository;
    protected $categoryRepository;
    protected $pageRepository;
    protected $imageRepository;
    protected $mapRepository;

    /**
     * EditorController constructor.
     * @param PostRepository $postRepository
     * @param CommentRepository $commentRepository
     * @param UserRepository $userRepository
     * @param CategoryRepository $categoryRepository
     * @param TagRepository $tagRepository
     * @param PageRepository $pageRepository
     * @param ImageRepository $imageRepository
     * @param MapRepository $mapRepository
     * @internal param MapRepository $mapRepository
     */
    public function __construct(PostRepository $postRepository,
                                CommentRepository $commentRepository,
                                UserRepository $userRepository,
                                CategoryRepository $categoryRepository,
                                TagRepository $tagRepository,
                                PageRepository $pageRepository,
                                ImageRepository $imageRepository,
                                MapRepository $mapRepository
    )
    {
        $this->postRepository = $postRepository;
        $this->commentRepository = $commentRepository;
        $this->userRepository = $userRepository;
        $this->categoryRepository = $categoryRepository;
        $this->tagRepository = $tagRepository;
        $this->pageRepository = $pageRepository;
        $this->imageRepository = $imageRepository;
        $this->mapRepository = $mapRepository;
    }

    public function index()
    {
        $info = [];
        $info['post_count'] = $this->postRepository->countByEditor(Auth::id());
        $info['image_count'] = $this->imageRepository->count();
        $postDetail = Post::select([
            DB::raw("YEAR(created_at) as year"),
            DB::raw("MONTH(created_at) as month"),
            DB::raw('COUNT(id) AS count'),
        ])->whereBetween('created_at', [Carbon::now()->subYear(1), Carbon::now()])
            ->groupBy('year', 'month')
            ->orderBy('year', 'asc')
            ->orderBy('month', 'asc')
            ->get()
            ->toArray();
        $labels = [];
        $data = [];
        foreach ($postDetail as $detail) {
            array_push($labels, $detail['year'] . '-' . $detail['month']);
            array_push($data, $detail['count']);
        }
        $response = view('editor.index', compact('info', 'labels', 'data'));
        if (($failed_jobs_count = DB::table('failed_jobs')->count()) > 0) {
            $failed_jobs_link = route('editor.failed-jobs');
            $response->withErrors(['failed_jobs' => "You have $failed_jobs_count failed jobs.<a href='$failed_jobs_link'>View</a>"]);
        }
        return $response;
    }

    public function settings()
    {
        $variables = config('configurable_variables');
        $groups = $variables['groups'];
        return view('editor.settings', compact('variables', 'groups'));
    }

    public function saveSettings(Request $request)
    {
        $inputs = $request->except('_token');
        $this->mapRepository->saveSettings($inputs);
        return back()->with('success', __('web.SAVE_SUCCESS'));
    }

    public function posts()
    {
        $posts = $this->postRepository->pagedPostsWithoutGlobalScopes();
        return view('editor.posts', compact('posts'));
    }

    public function flushFailedJobs()
    {
        $result = DB::table('failed_jobs')->delete();
        if ($result) {
            return back()->with('success', "Flush $result failed jobs");
        }
        return back()->withErrors("Flush failed jobs failed");
    }

}
