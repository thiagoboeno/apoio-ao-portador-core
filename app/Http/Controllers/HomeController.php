<?php

namespace App\Http\Controllers;

use App\Http\Repositories\PostRepository;
use App\Http\Repositories\UserRepository;
use App\Post;
use Illuminate\Http\Request;
use XblogConfig;

class HomeController extends Controller
{
    protected $postRepository;
    protected $userRepository;

    /**
     * Create a new controller instance.
     *
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository, UserRepository $userRepository)
    {
        $this->postRepository = $postRepository;
        $this->userRepository = $userRepository;
    }

    public function index()
    {
        $totalUsers = $this->userRepository->count();
        $users = $this->userRepository->countUsers();
        $enterprises = $this->userRepository->countEditor();
        $jobs = $this->postRepository->postCount();
        return view('index', compact('totalUsers', 'users', 'enterprises', 'jobs'));
    }

    public function search(Request $request)
    {
        $key = trim($request->get('q'));
        if ($key == '')
            return back()->withErrors(__('web.PLEASE_KEY_IN_KEYWORD'));
        $page_size = XblogConfig::getValue('page_size', 7);
        $key = "%$key%";
        $posts = Post::where('title', 'like', $key)
            ->orWhere('description', 'like', $key)
            ->with(['tags', 'category'])
            ->withCount('comments')
            ->orderBy('view_count', 'desc')
            ->paginate($page_size);
        $posts->appends($request->except('page'));
        return view('search', compact('posts'));
    }

    public function projects()
    {
        return view('projects');
    }

    public function achieve()
    {
        $posts = $this->postRepository->achieve();
        $posts_count = $this->postRepository->postCount();
        return view('achieve', compact('posts', 'posts_count'));
    }

}
